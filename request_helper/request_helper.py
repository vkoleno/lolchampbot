from typing import Any

import aiohttp
from fake_useragent import UserAgent

from logger.logger import logger


class RequestHelper:

    user_agent = UserAgent()

    async def request(
        self,
        url: str,
        method: str,
        query_params: dict | None = None,
        payload: dict | None = None,
        response_content_type: str = "json",
    ) -> Any:
        """
        Make HTTP request
        :param url: resource url
        :param method: http method
        :param query_params: GET request query params
        :param payload: POST request params
        :param response_content_type:
        :return: dict from response json
        """
        result: Any = ""
        async with aiohttp.ClientSession() as session:
            try:
                request_params: dict = self._get_request_params(method, query_params, payload)
                logger.info(f"Requesting {method} {url} with params: {query_params or payload}")
                async with session.request(method, url, **request_params) as response:
                    logger.info(f"Got response from {url} with status {response.status}")
                    result = await response.__getattribute__(response_content_type)()
            except aiohttp.ClientError as exc:
                logger.error(f"Failed to request {url}: {exc}")
        return result

    def _get_default_headers(self) -> dict:
        """
        Returns default headers part
        :return: base headers
        """
        return {"user-agent": self.user_agent.random}

    def _get_request_params(self, method: str, query_params: dict | None, payload: dict | None) -> dict:
        """
        Returns request params depending on http method
        :param method: http method
        :param query_params: GET request query params
        :param payload: POST request params
        :return: kwargs for calling aiohttp request methods
        """
        request_params: dict = {}
        request_params.update({"headers": self._get_default_headers()})
        if method == aiohttp.hdrs.METH_GET:
            if query_params:
                request_params.update({"query_params": query_params})
        elif method == aiohttp.hdrs.METH_POST:
            if payload:
                request_params.update({"data": payload})
        return request_params
