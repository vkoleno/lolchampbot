import os
from enum import Enum
from typing import TypeVar

from pydantic import BaseModel

TELEGRAM_TOKEN: str = os.getenv("TELEGRAM_TOKEN", "")  # telegram bot token obtained from BotFather
RIOT_API_KEY: str = ""

RIOT_API_HOST: str = "https://ru.api.riotgames.com/lol"
RIOT_CHAMP_API: str = "http://ddragon.leagueoflegends.com/cdn/{version}/data/{locale}/{entity}.json"
RIOT_VERSION_API: str = "https://ddragon.leagueoflegends.com/api/versions.json"
RIOT_ASSETS_API: str = "http://ddragon.leagueoflegends.com/cdn/10.12.1/img/{group}/{name}"

OPGG_HOST: str = "https://op.gg"

OPGG_HELPER_API_HOST: str = "http://127.0.0.1:8000"
OPGG_HELPER_API_AVAILABLE_POSITIONS_PATH: str = "/champions/{champion}/available_positions"
OPGG_HELPER_API_CHAMPION_POSITION_HELP_PATH: str = "/champions/{champion}/{position}"

DEFAULT_DB_FILENAME: str = "bot.db"  # Any

# riot api config
LOCALE: str = "en_US"  # Default locale, only one locale is supported atm

MATCHUP_AMOUNT_THRESHOLD: int = 5
MATCHUP_WINRATE_THRESHOLD: float = 0.5

CACHE_EXPIRATION_SECONDS: int = 600


class ItemSets(str, Enum):
    STARTER: str = "starter_items"
    CORE: str = "core_items"
    BOOTS: str = "boots"

    def __repr__(self):
        return str(self)

    def __str__(self):
        return self.value


class RepositoryType(str, Enum):
    SQLITE: str = "sqlite"


T = TypeVar("T", bound=BaseModel)


repository_source_config: dict = {
    "source_type": RepositoryType.SQLITE,
    "config": {
        "db_filename": "bot.db",
    },
}
