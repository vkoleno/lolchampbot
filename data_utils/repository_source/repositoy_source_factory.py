from typing import Any

from pydantic import BaseModel
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, async_sessionmaker, create_async_engine

from config import DEFAULT_DB_FILENAME, RepositoryType, repository_source_config


class RepositorySourceConfig(BaseModel):
    """Parser for data storage config."""

    source_type: str
    config: dict[str, Any]


def repository_source_factory() -> Any:
    """Returns object to interact with data source based on provided config."""
    config: RepositorySourceConfig = RepositorySourceConfig.parse_obj(repository_source_config)
    if config.source_type == RepositoryType.SQLITE:
        db_filename: str = config.config.get("db_filename") or DEFAULT_DB_FILENAME
        engine: AsyncEngine = create_async_engine(f"sqlite+aiosqlite:///{db_filename}")
        source: async_sessionmaker[AsyncSession] = async_sessionmaker(
            engine,
            expire_on_commit=False,
            class_=AsyncSession,
        )
        return source
