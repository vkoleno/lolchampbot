from abc import ABC, abstractmethod
from typing import Any


class AbstractChampionRepository(ABC):
    """Interface definition to access Champion's data."""

    source: Any

    def __init__(self, source: Any) -> None:
        self.source = source

    @abstractmethod
    async def get_file_id(self, champ: str, icon: str) -> str:
        """
        Returns file id of file uploaded to telegram servers for reusing image which was in use once
        :param champ: name of champ
        :param icon: name of db column with appropriate skill's icon
        :return: file id on telegram server to access it
        """
        raise NotImplementedError

    @abstractmethod
    async def write_file_id(self, champ: str, file_id: str, field_name: str) -> None:
        """
        When image is sent over bot, it is stored on telegram server and its id returned for reusage. Storing this id
        :param champ: simplified internal name
        :param file_id: file id on telegram servers
        :param field_name: name of fields for skill's image
        """
        raise NotImplementedError

    @abstractmethod
    async def write_champion_data(self, champion_data: dict) -> None:  # initialize champion in the db
        """
        Store requested champions' attributes in db for faster access further
        :param champion_data: base champion data (id, name, simplified internal name)
        """
        raise NotImplementedError
