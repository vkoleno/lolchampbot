from sqlalchemy import update
from sqlalchemy.dialects.sqlite import insert
from sqlalchemy.dialects.sqlite.dml import Insert
from sqlalchemy.engine import Result
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker
from sqlalchemy.future import select
from sqlalchemy.sql.expression import Select, Update

from models.db_models import Champion

from .abstract_champion_repository import AbstractChampionRepository


class SqliteChampionRepository(AbstractChampionRepository):

    def __init__(self, source: async_sessionmaker[AsyncSession]) -> None:
        super().__init__(source)

    async def get_file_id(self, champ: str, icon: str) -> str:
        """
        Returns file id of file uploaded to telegram servers for reusing image which was in use once
        :param champ: name of champ
        :param icon: name of db column with appropriate skill's icon
        :return: file id on telegram server to access it
        """
        async with self.source() as session:
            stmt: Select = select(Champion.__dict__[icon]).where(Champion.internal_name == champ)
            result: Result = await session.execute(stmt)
            return result.scalars().one()

    async def write_file_id(self, champ: str, file_id: str, field_name: str) -> None:
        """
        When image is sent over bot, it is stored on telegram server and its id returned for reusage. Storing this id
        :param champ: simplified internal name
        :param file_id: file id on telegram servers
        :param field_name: name of fields for skill's image
        """
        async with self.source() as session:
            async with session.begin():
                insert_stmt: Update = (
                    update(Champion)
                    .where(Champion.internal_name == champ)
                    .values(
                        **{field_name: file_id},
                    )
                )
                await session.execute(insert_stmt)

    async def write_champion_data(self, champion_data: dict) -> None:  # initialize champion in the db
        """
        Store requested champions' attributes in db for faster access further
        :param champion_data: base champion data (id, name, simplified internal name)
        """
        async with self.source() as session:
            async with session.begin():
                insert_stmt: Insert = insert(Champion).values(
                    id=champion_data["key"],
                    champ_name=champion_data["name"],
                    internal_name=champion_data["id"],
                )
                do_nothing_stmt: Insert = insert_stmt.on_conflict_do_nothing(index_elements=[Champion.id])
                await session.execute(do_nothing_stmt)
