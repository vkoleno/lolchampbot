from sqlalchemy.dialects.sqlite import insert
from sqlalchemy.dialects.sqlite.dml import Insert
from sqlalchemy.engine import Result
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker
from sqlalchemy.future import select
from sqlalchemy.sql.expression import Select

from models.db_models import User

from .abstract_user_repository import AbstractUserRepository


class SQLiteUserRepository(AbstractUserRepository):

    def __init__(self, source: async_sessionmaker[AsyncSession]) -> None:
        super().__init__(source)

    async def get_current_selected_champion(self, user_id: int) -> str:
        """
        Returns champion name which user requested last
        :param user_id: telegram user id
        :return: champion name
        """
        async with self.source() as session:
            stmt: Select = select(User.selected_champ).where(User.id == user_id)
            result: Result = await session.execute(stmt)
            return result.scalars().one()

    async def write_selected_champion(self, user_id: int, champ_name: str) -> None:
        """
        Remember last user's selected champion
        :param user_id: telegram user id
        :param champ_name: simplified internal champion name
        """
        async with self.source() as session:
            async with session.begin():
                insert_stmt: Insert = insert(User).values(selected_champ=champ_name, id=user_id)
                do_update_stmt: Insert = insert_stmt.on_conflict_do_update(
                    index_elements=[User.id],
                    set_=dict(selected_champ=champ_name),
                )
                await session.execute(do_update_stmt)

    async def get_current_selected_lane(self, user_id: int) -> str:
        """
        Returns lane name which user requested last
        :param user_id: telegram user id
        :return: lane name
        """
        async with self.source() as session:
            stmt: Select = select(User.lane).where(User.id == user_id)
            result: Result = await session.execute(stmt)
            return result.scalars().one()

    async def write_selected_lane(self, user_id: int, lane: str) -> None:
        """
        Remember last user's selected lane
        :param user_id: telegram user id
        :param lane: lane name
        """
        async with self.source() as session:
            async with session.begin():
                insert_stmt: Insert = insert(User).values(lane=lane, id=user_id)
                do_update_stmt: Insert = insert_stmt.on_conflict_do_update(
                    index_elements=[User.id],
                    set_=dict(lane=lane),
                )
                await session.execute(do_update_stmt)
