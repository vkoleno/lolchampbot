from abc import ABC, abstractmethod
from typing import Any


class AbstractUserRepository(ABC):
    """Interface definition to access User's data."""

    source: Any

    def __init__(self, source: Any) -> None:
        self.source = source

    @abstractmethod
    async def get_current_selected_champion(self, user_id: int) -> str:
        """
        Returns the champion name which user has requested last
        :param user_id: telegram user id
        :return: champion name
        """
        raise NotImplementedError

    @abstractmethod
    async def get_current_selected_lane(self, user_id: int) -> str:
        """
        Returns lane name which user has requested last
        :param user_id: telegram user id
        :return: lane name
        """
        raise NotImplementedError

    @abstractmethod
    async def write_selected_champion(self, user_id: int, champ_name: str) -> None:
        """
        Store last user's selected champion
        :param user_id: telegram user id
        :param champ_name: simplified internal champion name
        """
        raise NotImplementedError

    @abstractmethod
    async def write_selected_lane(self, user_id: int, lane: str) -> None:
        """
        Store last user's selected lane
        :param user_id: telegram user id
        :param lane: lane name
        """
        raise NotImplementedError
