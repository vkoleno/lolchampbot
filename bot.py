import asyncio
import string
from typing import Any

import aiohttp
from aiogram import Bot, Dispatcher, F
from aiogram.client.default import DefaultBotProperties
from aiogram.enums import ParseMode
from aiogram.types import CallbackQuery, Message
from aiogram.utils.keyboard import InlineKeyboardBuilder
from pydantic import parse_obj_as

import config
from data_utils.repository import (
    AbstractChampionRepository,
    AbstractUserRepository,
    SqliteChampionRepository,
    SQLiteUserRepository,
)
from data_utils.repository_source.repositoy_source_factory import repository_source_factory
from logger.logger import logger
from models.models import ChampionHelp
from request_helper.request_helper import RequestHelper
from riot_api_helper import RiotApiHelper

bot: Bot = Bot(token=config.TELEGRAM_TOKEN)
dispatcher: Dispatcher = Dispatcher()
request_helper: RequestHelper = RequestHelper()
riot_helper: RiotApiHelper = RiotApiHelper()
source: Any = repository_source_factory()
user_repository: AbstractUserRepository = SQLiteUserRepository(source)
champion_repository: AbstractChampionRepository = SqliteChampionRepository(source)


@dispatcher.message(F.text.regexp("^/start$"))
async def send_alphabet_keyboard(message: Message) -> None:
    """
    Make navigation in the champions list - first ask user for the first letter on champion name and hide
    alphabet keyboard until request
    :param message: payload of user's message
    """
    kb_builder: InlineKeyboardBuilder = InlineKeyboardBuilder()
    for letter in list(string.ascii_uppercase):
        kb_builder.button(text=letter, callback_data=letter)
    await bot.send_message(
        message.chat.id,
        "Please select first letter on the champion's name",
        reply_markup=kb_builder.as_markup(),
    )


@dispatcher.callback_query(F.data.in_(string.ascii_uppercase))
async def send_champions_keyboard(query: CallbackQuery) -> None:
    """
    Send selector of champions' names starts from selected letter
    :param query: payload of user's message
    """
    champions: dict = riot_helper.champions_data
    match_champs_data: list[list] = [
        [champions[champ]["name"], champ] for champ in champions if champions[champ]["name"].startswith(query.data)
    ]

    kb_builder: InlineKeyboardBuilder = InlineKeyboardBuilder()
    for champ_data in match_champs_data:
        kb_builder.button(text=champ_data[0], callback_data=champ_data[1])
    add_back_to_alphabet_button(kb_builder)
    await bot.send_message(query.from_user.id, "Please select a champion", reply_markup=kb_builder.as_markup())


@dispatcher.callback_query(lambda query: query.data in riot_helper.champions_data)
async def send_lane_selector(query: CallbackQuery) -> None:
    """
    Returns selector of lanes typically used by selected champion
    :param query: payload of user's query
    """
    chat_id: int = query.from_user.id

    champ_name: str | None = query.data
    if champ_name:
        await user_repository.write_selected_champion(chat_id, champ_name)

    available_lanes: list[str] = await request_helper.request(
        config.OPGG_HELPER_API_HOST + config.OPGG_HELPER_API_AVAILABLE_POSITIONS_PATH.format(champion=champ_name),
        aiohttp.hdrs.METH_GET,
    )
    if available_lanes:
        kb_builder: InlineKeyboardBuilder = InlineKeyboardBuilder()
        for lane in available_lanes:
            kb_builder.button(text=lane.capitalize(), callback_data=lane.lower())
        await bot.send_message(chat_id, "Please select a lane", reply_markup=kb_builder.as_markup())
    else:
        logger.warning(f"Failed to fetch available lanes, user {chat_id}, champion: {champ_name}")
        await send_nothing_found(query, "Failed to fetch available lanes, please try again later")


@dispatcher.callback_query(lambda query: query.data in ("top", "adc", "support", "mid", "jungle"))
async def send_champion_options(query: CallbackQuery) -> None:
    """
    Send available data user can request: skillset, skillorder
    :param query: payload of user's request
    """
    chat_id: int = query.from_user.id
    query_data: str | None = query.data

    if query_data:
        await user_repository.write_selected_lane(query.from_user.id, query_data)

    champions: dict = riot_helper.champions_data
    champ_name: str = await user_repository.get_current_selected_champion(chat_id)
    lane: str = await user_repository.get_current_selected_lane(chat_id)

    await champion_repository.write_champion_data(champions[champ_name])
    # TODO: add guides, itemization, abilities leveling priority
    kb_builder: InlineKeyboardBuilder = InlineKeyboardBuilder()
    kb_builder.button(text="Skillset", callback_data="skillset")
    kb_builder.button(text="Skill Order", callback_data="skillorder")
    add_back_to_alphabet_button(kb_builder)

    champion_help: ChampionHelp = await get_champion_help(champ_name, lane)
    if champion_help:
        winrate_raw: float = champion_help.champion_win_rate
        winrate: float = round(winrate_raw * 100, 2)

        caption: str = "{} (winrate {}%)".format(champions[champ_name]["name"], winrate)
        await send_image(chat_id, champ_name, "champ", caption, kb_builder)
    else:
        logger.warning(f"Failed to send champion options, user {chat_id}, champion: {champ_name}, {lane}")
        await send_nothing_found(query, "Failed to fetch champion info, please try again later")


@dispatcher.callback_query(lambda query: query.data == "back_to_alphabet")
async def back_to_alphabet_kb(query: CallbackQuery) -> None:
    """
    Return user to the initial screen by request
    :param query: payload of user's request
    """
    if query.data == "back_to_alphabet":
        await send_alphabet_keyboard(query.message)


@dispatcher.callback_query(lambda query: query.data == "skillset")  # return inline kb with abilities
async def send_skillset_options(query: CallbackQuery) -> None:
    """
    Sends passive description and selector for requesting others champions abilities
    :param query: payload of user's request
    """
    chat_id: int = query.from_user.id

    skillset: dict = await get_current_champion_skillset(query)
    query_data: str | None = query.data
    if skillset and query_data:
        lane: str = await user_repository.get_current_selected_lane(chat_id)

        caption: str = "Passive: {passive_name} - {passive_description}".format(
            passive_name=skillset["passive"][0],
            passive_description=skillset["passive"][1],
        )
        kb_builder: InlineKeyboardBuilder = InlineKeyboardBuilder()
        add_skills_buttons(kb_builder, query_data)
        add_inline_back_to_champ_screen_kb(kb_builder, lane)

        champ_name: str = await user_repository.get_current_selected_champion(chat_id)
        await send_image(chat_id, champ_name, "passive", caption, kb_builder)
    else:
        logger.warning(f"Failed to send skillset options, user {chat_id}")
        await send_nothing_found(query, "None champion selected")


@dispatcher.callback_query(lambda query: query.data == "skillorder")
async def send_skill_order(query: CallbackQuery) -> None:
    """
    Sends preferred order of skills leveling
    :param query: payload of user's request
    """
    chat_id: int = query.from_user.id
    champ_name: str = await user_repository.get_current_selected_champion(chat_id)
    lane: str = await user_repository.get_current_selected_lane(chat_id)
    # check if we already have skill order for the champion in the db; otherwise request it from riot API
    champion_help: ChampionHelp = await get_champion_help(champ_name, lane)
    if champion_help:
        skill_order: str = champion_help.skill_order.order
        kb_builder: InlineKeyboardBuilder = InlineKeyboardBuilder()
        add_inline_back_to_champ_screen_kb(kb_builder, lane)
        await bot.send_message(chat_id, text=skill_order, reply_markup=kb_builder.as_markup())
    else:
        logger.warning(
            f"Failed to send skill order, user {chat_id}, champion: {champ_name}, champion help: {champion_help}",
        )
        await send_nothing_found(query, "Failed to fetch champion data, please try again later")


@dispatcher.callback_query(lambda query: query.data in ["q", "w", "e", "r"])
async def send_skill_info(query: CallbackQuery) -> None:
    """
    Send description of requested skill
    :param query: payload of user's request
    """
    chat_id: int = query.from_user.id
    champ_name: str = await user_repository.get_current_selected_champion(chat_id)
    lane: str = await user_repository.get_current_selected_lane(chat_id)

    skillset: dict = await get_current_champion_skillset(query)
    query_data: str | None = query.data
    if skillset and query_data:
        skill_name: str = skillset[query_data][0]
        skill_desc: str = skillset[query_data][1]

        caption: str = "{}: {} - {}".format(query_data.upper(), skill_name, skill_desc)
        kb_builder: InlineKeyboardBuilder = InlineKeyboardBuilder()
        add_skills_buttons(kb_builder, query_data)
        add_inline_back_to_champ_screen_kb(kb_builder, lane)
        await send_image(chat_id, champ_name, query_data, caption, kb_builder)
    else:
        logger.warning(f"Failed to skill info, user {chat_id}, champion: {champ_name}, skillset: {skillset}")
        await send_nothing_found(query, "None champion selected")


def add_back_to_alphabet_button(kb_builder: InlineKeyboardBuilder) -> None:
    """
    Adds back to alphabet button to given kb
    :param kb_builder: keyboard to be enhanced
    """
    kb_builder.button(text="Back to alphabet selector", callback_data="back_to_alphabet")


def add_inline_back_to_champ_screen_kb(kb_builder: InlineKeyboardBuilder, lane: str) -> None:
    """
    Adds back to champion initial screen
    :param lane: Actual selected lane
    :param kb_builder: keyboard to be enhanced
    """
    kb_builder.button(text="Back to champion", callback_data=lane)


def add_skills_buttons(kb_builder: InlineKeyboardBuilder, current_skill: str) -> None:
    skills: list[str] = ["q", "w", "e", "r"]
    for skill in skills:
        if skill != current_skill:
            kb_builder.button(text=skill.upper(), callback_data=skill)


async def get_current_champion_skillset(query: CallbackQuery) -> dict:
    """
    Returning skillset for champion which was last selected for the user
    :param query: payload of user's request
    :return: information about champ skills
    """
    skillset_dict: dict = {}
    chat_id: int = query.from_user.id
    try:
        champ_name: str = await user_repository.get_current_selected_champion(chat_id)
        champ_data: dict = riot_helper.champions_data[champ_name]

        skillset: list[list] = [
            [
                champ_data["passive"]["name"],
                champ_data["passive"]["description"],
                champ_data["passive"]["image"]["full"],
            ],
        ]
        for skill in champ_data["spells"]:
            skillset.append([skill["name"], skill["description"], skill["image"]["full"]])
        skillset_dict = {
            "passive": skillset[0],
            "q": skillset[1],
            "w": skillset[2],
            "e": skillset[3],
            "r": skillset[4],
        }
    except TypeError:
        logger.error(f"Failed to fetch skillset for user {chat_id}, champion or db record is missing")

    return skillset_dict


async def send_image(
    chat_id: int,
    champ_name: str,
    entity: str,
    caption: str,
    kb_builder: InlineKeyboardBuilder,
) -> None:
    """
    Sends an image of champion, spell or passive depending on existence of file_id in db - otherwise use image url
    and save file_id
    :param chat_id: user (chat) id
    :param champ_name: internal champ name
    :param entity: 'champion', 'passive' or q, w, e, r
    :param caption: a message about an image
    :param kb_builder : telegram inline keyboard
    :return: None
    """
    file_id: str = await champion_repository.get_file_id(champ_name, "{}_icon".format(entity))
    group: str = riot_helper.get_image_group(champ_name, entity)
    name: str = riot_helper.get_image_name(champ_name, group, entity)
    if file_id is not None:
        photo: str = file_id
    else:
        photo = riot_helper.get_image_url(group, name)
    send_photo_callback: Message = await bot.send_photo(
        chat_id,
        photo=photo,
        caption=caption,
        reply_markup=kb_builder.as_markup(),
    )
    if file_id is None:
        file_id = send_photo_callback.photo[0].file_id
        await champion_repository.write_file_id(champ_name, file_id, "{}_icon".format(entity))


async def send_nothing_found(query: CallbackQuery, message_text: str) -> None:
    """
    Respond with fallback
    :param query: user message's query
    :param message_text: text to return for user
    """
    await bot.send_message(query.from_user.id, message_text)
    await send_alphabet_keyboard(query.message)


async def get_champion_help(champ_name: str, position: str) -> ChampionHelp:
    champion_help_response: dict = await request_helper.request(
        "{host}{path}".format(
            host=config.OPGG_HELPER_API_HOST,
            path=config.OPGG_HELPER_API_CHAMPION_POSITION_HELP_PATH.format(
                champion=champ_name,
                position=position,
            ),
        ),
        aiohttp.hdrs.METH_GET,
    )
    return parse_obj_as(ChampionHelp, champion_help_response)


async def main() -> None:
    bot = Bot(token=config.TELEGRAM_TOKEN, default=DefaultBotProperties(parse_mode=ParseMode.HTML))
    await dispatcher.start_polling(bot)


if __name__ == "__main__":
    logger.info("Starting bot")
    asyncio.run(main())
