import logging
import os
from logging.handlers import RotatingFileHandler

logging.basicConfig(
    handlers=[
        RotatingFileHandler(os.path.join(os.getcwd(), "logs", "my_log.log"), maxBytes=100000, backupCount=10),
        logging.StreamHandler(),
    ],
    level=logging.DEBUG,
    format="[%(asctime)s] %(levelname)s [%(name)s.%(funcName)s:%(lineno)d] %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S",
)

logger: logging.Logger = logging.getLogger()
