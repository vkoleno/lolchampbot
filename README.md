# About
Bot is intended to help League of Legends players to get information about champions (theirs, opponents, allies) - for selecting counter picks, buys, skill leveling from their mobile phones while game is loading

# Setup
1. Install poetry https://python-poetry.org/docs/#installation
2. Install dependencies:  
```poetry install```
3. Install pre-commit hooks:  
```pre-commit install```
4. Apply migrations:  
```alembic upgrade head```
5. Obtain telegram bot token from @BotFather - open https://t.me/BotFather and follow instructions
6. Set environment variable TELEGRAM_TOKEN with the token from previous bullet

# Run project
1. Run op.gg helper API:  
```uvicorn opgg_data_helper:app```
2. Run bot itself:  
```python bot.py```
# TODO
https://stellar-factory-d84.notion.site/3a2746a39d334422bdb47809d8cdf4a6?v=6e8a7d6ed42d419ab4d6e73b1a435598