from pydantic import BaseModel


class Champion(BaseModel):
    champion_id: int
    play: int
    win: int
    winrate: float
    image_url: str
    key: str
    name: str

    def __init__(self, **kwargs) -> None:
        meta: dict = kwargs.get("meta", {})
        if "image_url" not in kwargs:
            kwargs["image_url"] = meta.get("image_url")
        if "key" not in kwargs:
            kwargs["key"] = meta.get("key")
        if "name" not in kwargs:
            kwargs["name"] = meta.get("name")
        super().__init__(**kwargs)


class Item(BaseModel):
    id: int
    name: str
    image_url: str


class Items(BaseModel):
    starter_items: list[Item]
    core_items: list[Item]
    boots: Item | None


class SkillOrder(BaseModel):
    priority: list[str]
    order: str


class SummonerSpell(BaseModel):
    id: int
    key: str
    name: str
    image_url: str


class ChampionHelp(BaseModel):
    summoner_spells: list[SummonerSpell]
    skill_order: SkillOrder
    items: Items
    runes: dict
    champion_win_rate: float
    counters: list[Champion]
    countered_by: list[Champion]


class Position(BaseModel):
    name: str
