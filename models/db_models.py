from sqlalchemy import Column, Integer, String, text
from sqlalchemy.orm import DeclarativeBase


class Base(DeclarativeBase):
    pass


class Champion(Base):
    __tablename__ = "champions"

    id = Column(Integer, primary_key=True)
    champ_id = Column(Integer, unique=True)
    champ_name = Column(String(255), server_default=text("NULL"))
    internal_name = Column(String(30), server_default=text("NULL"))
    champ_icon = Column(String(255), server_default=text("NULL"))
    q_icon = Column(String(255), server_default=text("NULL"))
    w_icon = Column(String(255), server_default=text("NULL"))
    e_icon = Column(String(255), server_default=text("NULL"))
    r_icon = Column(String(255), server_default=text("NULL"))
    passive_icon = Column(String(255), server_default=text("NULL"))


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    selected_champ = Column(String(50), server_default=text("NULL"))
    lane = Column(String(10), server_default=text("NULL"))
