from copy import deepcopy
from datetime import datetime
from typing import Type

from cache.base_cache import BaseCache
from config import CACHE_EXPIRATION_SECONDS, T
from utils.singleton import Singleton


class InMemoryCache(BaseCache, metaclass=Singleton):
    """
    Simple cache without size control considering that expected total amount of data relatively low and it will be used
    in single thread and process
    """

    TIMESTAMP_FIELD_NAME: str = "creation_timestamp"

    def __init__(self) -> None:
        self._cache: dict = {}

    def get(self, key: str, result_model: Type[T] | None = None) -> dict | T | None:
        raw_value: dict | None = self._cache.get(key)
        result: dict | T | None = None
        if raw_value:
            raw_value = deepcopy(raw_value)
            is_record_expired: bool = self._is_record_expired(raw_value)
            if not is_record_expired:
                del raw_value[self.TIMESTAMP_FIELD_NAME]
                result = raw_value if result_model is None else result_model.parse_obj(raw_value)
            else:
                del self._cache[key]
                result = None
        return result

    def put(self, key: str, value: dict | T) -> None:
        value = deepcopy(value)
        if not isinstance(value, dict):
            value = dict(value)
        value[self.TIMESTAMP_FIELD_NAME] = int(datetime.now().timestamp())
        self._cache[key] = value

    def reset(self) -> None:
        self._cache = {}

    def _is_record_expired(self, value: dict) -> bool:
        result: bool = True
        creation_timestamp: int | None = value.get(self.TIMESTAMP_FIELD_NAME)
        if creation_timestamp:
            result = creation_timestamp + CACHE_EXPIRATION_SECONDS <= int(datetime.now().timestamp())
        return result
