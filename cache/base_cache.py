from abc import ABC, abstractmethod
from typing import Type

from config import T


class BaseCache(ABC):

    @abstractmethod
    def get(self, key: str, result_model: Type[T] | None = None) -> dict | T | None:
        """
        Get value by its key
        :param key: cache record identifier
        :param result_model: pydantic model to convert dict result into
        :return: value from cache
        """
        pass

    @abstractmethod
    def put(self, key: str, value: dict | T) -> None:
        """
        Put value in cache
        :param key: cache record identifier
        :param value: dict or pydantic model to put in cache as json
        """
        pass

    @abstractmethod
    def reset(self) -> None:
        """
        Reset cache
        """
        pass
