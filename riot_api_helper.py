import requests

import config
from logger.logger import logger


class RiotApiHelper:

    def __init__(self) -> None:
        self.riot_champ_api: str = config.RIOT_CHAMP_API
        self.riot_version_api: str = config.RIOT_VERSION_API
        self.assets_api: str = config.RIOT_ASSETS_API
        self.locale: str = config.LOCALE
        self.version: str = self.get_version()
        self.champions_data: dict = self.get_champions()
        self.spell_default_index: dict = {"q": 0, "w": 1, "e": 2, "r": 3}

    def get_version(self) -> str:
        """
        Get actual game client version
        :return: Actual game client version
        """
        game_version: str = requests.get(self.riot_version_api).json()[0]
        logger.info(f"Game version: {game_version}")
        return game_version

    def get_champions(self) -> dict:
        """
        Get full champions list
        :return: dict with base champions data
        """
        entity: str = "championFull"
        url: str = self.riot_champ_api.format(version=self.version, locale=self.locale, entity=entity)
        logger.info("Fetching static champions data")
        response: dict = requests.get(url).json()["data"]
        logger.info("Static champions data fetched")
        return response

    def get_image_url(self, group: str, name: str) -> str:
        """
        Get url of image asset based on its type (champion, skill, passive icon)
        :param group: type of image
        :param name: name of image file
        :return: url of image file
        """
        return self.assets_api.format(version=self.version, group=group, name=name)

    def get_image_name(self, champ_name: str, group: str, entity: str) -> str:
        """
        Get image name to construct url
        :param champ_name: name of champion
        :param group: type of image
        :param entity: skill's letter
        :return: file name
        """
        image: str = ""
        if group == "champion":
            image = self.champions_data[champ_name]["image"]["full"]
        elif group == "passive":
            image = self.champions_data[champ_name]["passive"]["image"]["full"]
        elif group == "spell":
            image = self.champions_data[champ_name]["spells"][self.spell_default_index[entity]]["image"]["full"]
        return image

    def get_image_group(self, champ_name: str, entity: str) -> str:
        """
        Some hard to understand classification of images depending on entity, just get it
        :param champ_name: champion name
        :param entity: skill's letter
        :return: type of image to construct url
        """
        image_group: str = ""
        if entity == "passive":
            image_group = self.champions_data[champ_name]["passive"]["image"]["group"]
        elif entity in self.spell_default_index:
            image_group = self.champions_data[champ_name]["spells"][self.spell_default_index[entity]]["image"]["group"]
        elif entity == "champ":
            image_group = self.champions_data[champ_name]["image"]["group"]
        return image_group
