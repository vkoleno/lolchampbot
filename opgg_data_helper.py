import json
import operator
from typing import Callable

import aiohttp
from bs4 import BeautifulSoup
from dict_deep import deep_get
from fastapi import APIRouter, FastAPI
from fastapi_utils.cbv import cbv
from pydantic import parse_obj_as

import config
from cache.inmemory_cache import InMemoryCache
from config import (
    MATCHUP_AMOUNT_THRESHOLD,
    MATCHUP_WINRATE_THRESHOLD,
    OPGG_HELPER_API_AVAILABLE_POSITIONS_PATH,
    OPGG_HELPER_API_CHAMPION_POSITION_HELP_PATH,
    ItemSets,
)
from logger.logger import logger
from models.models import Champion, ChampionHelp, Item, Items, Position, SkillOrder, SummonerSpell
from request_helper.request_helper import RequestHelper

app: FastAPI = FastAPI()
router: APIRouter = APIRouter()


@cbv(router)
class OpggDataHelper(FastAPI):

    def __init__(self) -> None:
        self.opgg_host: str = config.OPGG_HOST
        self.cache: InMemoryCache = InMemoryCache()
        self.request_helper: RequestHelper = RequestHelper()
        super().__init__()

    @router.get(OPGG_HELPER_API_AVAILABLE_POSITIONS_PATH)
    async def get_opgg_available_positions(self, champion: str = "aatrox") -> list[str]:
        """Some champs has few available popular positions. Getting by champ name to show
        :param champion: champion name
        :return: list of positions for champion
        """
        available_positions: list[str] = []
        champion_url: str = self.opgg_host + "/champions/{}".format(champion)

        champ_page_content: str = await self.request_helper.request(
            champion_url,
            aiohttp.hdrs.METH_GET,
            response_content_type="text",
        )

        if champ_page_content:
            champion_data: dict = self._get_brief_champ_data(champ_page_content)

            positions_data_raw: list[dict] = deep_get(champion_data, "props.pageProps.data.summary.summary.positions")
            positions_data: list[Position] = parse_obj_as(list[Position], positions_data_raw)
            if positions_data:
                available_positions = [position.name for position in positions_data]

        return available_positions

    @router.get(OPGG_HELPER_API_CHAMPION_POSITION_HELP_PATH, response_model=ChampionHelp)
    async def get_opgg_champion_help_by_postition(
        self,
        champion: str,
        position: str,
    ) -> dict | ChampionHelp | None:
        """Parse champion's lane data and store in cache
        :param champion: champion name
        :param position: lane name
        :return: dict with parsed lane data
        """
        champion_help: dict | ChampionHelp | None = self.cache.get(
            self._get_champ_position_cache_key(champion, position),
            ChampionHelp,
        )
        if champion_help is None:
            data_url: str = self.opgg_host + "/champions/{}/{}/build".format(champion, position)
            champ_page_content: str = await self.request_helper.request(
                data_url,
                aiohttp.hdrs.METH_GET,
                response_content_type="text",
            )
            if champ_page_content:
                champion_data: dict = self._get_brief_champ_data(champ_page_content)

                summoner_spells: list[SummonerSpell] = self._get_summoner_spells_data(champion_data)
                skill_order: SkillOrder = self._get_skill_order(champion_data)
                items: Items = self._get_items_sets(champion_data)
                runes: dict = deep_get(champion_data, "props.pageProps.data.rune_pages")[0]
                champion_win_rate: float = deep_get(
                    champion_data,
                    "props.pageProps.data.summary.summary.average_stats.win_rate",
                )
                matchups_data: dict = self._get_matchups_data(champion_data)

                champion_help = ChampionHelp(
                    **{
                        "summoner_spells": summoner_spells,
                        "skill_order": skill_order,
                        "items": items,
                        "runes": runes,
                        "champion_win_rate": champion_win_rate,
                        **matchups_data,
                    },
                )

                self.cache.put(self._get_champ_position_cache_key(champion, position), champion_help)

        return champion_help

    def _get_matchups_data(self, champion_data: dict) -> dict:
        """
        Parse most popular matchups winrate
        :param champion_data: json with champion's information
        :return: whom counters the champion and who counters him
        """
        raw_counters: list[dict] = deep_get(champion_data, "props.pageProps.data.summary.counters") or []

        counters_winrate: dict = dict()
        for counter in raw_counters:
            winrate: float = round(counter.get("win", 0) / counter.get("play", 1), 2)
            counter["winrate"] = winrate
            counters_winrate[counter.get("champion_id")] = counter

        base_counters: list[Champion] = parse_obj_as(list[Champion], raw_counters)

        counters_winrate_sorted: list[Champion] = sorted(
            base_counters,
            key=lambda cou: cou.winrate,
            reverse=True,
        )

        counters: list[Champion] = self._filter_matchups_by_winrate_threshold(counters_winrate_sorted, operator.gt)
        countered_by: list[Champion] = self._filter_matchups_by_winrate_threshold(counters_winrate_sorted, operator.lt)

        return {
            "counters": counters[:MATCHUP_AMOUNT_THRESHOLD],
            "countered_by": countered_by[:MATCHUP_AMOUNT_THRESHOLD],
        }

    def _get_items_sets(self, champion_data: dict) -> Items:
        """Get most popular items sets for champion
        :param champion_data: all champion data
        :return: initial buy, most important items to buy for late game, boots upgrade
        """
        items_info: list[Item] = self._get_items_info_from_champion_data(champion_data)

        boots_variants: dict = deep_get(champion_data, "props.pageProps.data.boots")
        boots: Item | None = None  # TODO: cassi
        try:
            boots_id: int = boots_variants[0].get("ids")[0]  # TODO handle exceptions
            boots = self._get_item_from_list_by_id(items_info, boots_id)
        except (TypeError, IndexError):
            logger.error(f"Failed to get boots data. Items info: {items_info}, boots_variants: {boots_variants}")

        starter_items: list[Item] = self._get_champion_itemset(champion_data, ItemSets.STARTER)
        core_items: list[Item] = self._get_champion_itemset(champion_data, ItemSets.CORE)
        items: Items = Items(
            starter_items=starter_items,
            core_items=core_items,
            boots=boots,
        )
        return items

    def _get_champion_itemset(self, champion_data: dict, itemset_name: str) -> list[Item]:
        """
        :param champion_data: all champion data
        :param itemset_name: starter, core, boots
        :return: items related to requested item set
        """
        items: list[Item] = []
        items_info: list[Item] = self._get_items_info_from_champion_data(champion_data)
        items_variants: list[dict] = deep_get(champion_data, "props.pageProps.data.{}".format(itemset_name))

        try:
            items_ids: list[int] = items_variants[0].get("ids", [])
            for item_id in items_ids:
                item: Item | None = self._get_item_from_list_by_id(items_info, item_id)
                if item:
                    items.append(item)
        except (IndexError, TypeError):
            logger.error(f"Failed to get itemset: {items_variants}")

        return items

    def _get_skill_order(self, champion_data: dict) -> SkillOrder:
        """Get champion's most efficient skills leveling order
        :param champion_data: all champion data
        :return: common abilities priority and leveling order from 1 to 18th level
        """
        skill_order_stats: dict = deep_get(champion_data, "props.pageProps.data.skill_masteries")
        skill_order: SkillOrder = SkillOrder(  # TODO: evolving skills
            priority=self._get_skill_ids_priority(skill_order_stats),
            order=self._get_skills_order(skill_order_stats),
        )
        return skill_order

    @staticmethod
    def _filter_matchups_by_winrate_threshold(
        matchups_list: list[Champion],
        comparison_operator: Callable,
    ) -> list[Champion]:
        """Filter matchups that doesn't meet requirement to be 'counters' or 'counter'
        :param matchups_list: list of champions expected to be counter or to counter
        :param comparison_operator: lt for counters, gt for counter
        :return: definitely counters or counter
        """
        return list(
            filter(
                lambda matchup: comparison_operator(matchup.winrate, MATCHUP_WINRATE_THRESHOLD),
                matchups_list,
            ),
        )

    @staticmethod
    def _get_summoner_spells_data(champion_data: dict) -> list[SummonerSpell]:
        """Get information about most popular summoner spell combination (two spells)
        :param champion_data: all champion data
        :return: two most popular summoner spells
        """
        summoner_spells_stats: dict = deep_get(champion_data, "props.pageProps.data.summoner_spells")
        summoner_spells_data_raw: list[dict] = deep_get(champion_data, "props.pageProps.data.meta.spells")
        champ_summoner_spells: list[SummonerSpell] = []

        try:
            summoner_spells_ids: list = summoner_spells_stats[0].get("ids")
            summoner_spells_data: list[SummonerSpell] = parse_obj_as(list[SummonerSpell], summoner_spells_data_raw)
            for summoner_spell_data in summoner_spells_data:
                if summoner_spell_data.id in summoner_spells_ids:
                    champ_summoner_spells.append(summoner_spell_data)
        except (IndexError, TypeError):
            logger.error(f"Failed to get summoner spells: {summoner_spells_stats}")
        return champ_summoner_spells

    @staticmethod
    def _get_skills_order(skill_order_stats: dict) -> str:
        """Get skill leveling order from lvl 1 to 18
        :param skill_order_stats: info about champ skill order
        :return: full skills leveling route
        """
        order: str = ""
        try:
            order = " > ".join(skill_order_stats[0].get("builds")[0].get("order"))
        except (IndexError, TypeError):
            logger.error(f"Failed to get skill order: {skill_order_stats}")
        return order

    @staticmethod
    def _get_skill_ids_priority(skill_order_stats: dict) -> list[str]:
        """Get base skills leveling priority
        :param skill_order_stats: info about champ skill order
        :return: abilities priority
        """
        ids: list[str] = []
        try:
            ids = skill_order_stats[0].get("ids", [])
        except (IndexError, TypeError):
            logger.error(f"Failed to get skill priority: {skill_order_stats}")
        return ids

    @staticmethod
    def _get_brief_champ_data(page_content: str) -> dict:
        """Get json data about champion from initially loaded page content
        :param page_content: initially loaded page content
        :return: champion's data
        """
        champ_page_soup: BeautifulSoup = BeautifulSoup(page_content, "html.parser")
        champion_data: dict = {}
        try:
            champion_data = json.loads(champ_page_soup.select("#__NEXT_DATA__")[0].contents[0])
        except (IndexError, AttributeError):
            logger.error("Failed to get json champ stats")
        return champion_data

    @staticmethod
    def _get_item_from_list_by_id(items: list[Item], item_id: int) -> Item | None:
        """Get requested items from list of dicts
        :param items: all items related to champion's loadouts
        : param item: id of requested item
        :return: details about requested items
        """
        result: Item | None = None
        for item in items:
            if item.id == item_id:
                result = item
                break
        return result

    @staticmethod
    def _get_items_info_from_champion_data(champion_data: dict) -> list[Item]:
        """Get items descriptions
        :param champion_data: all champion data
        :return: Information about items related to champion's recommended loadout
        """
        items_info_raw: list[dict] = deep_get(champion_data, "props.pageProps.data.meta.items") or []
        items_info: list[Item] = parse_obj_as(list[Item], items_info_raw)
        return items_info

    @staticmethod
    def _get_champ_position_cache_key(champ_name: str, position: str) -> str:
        """Get key for data in cache
        :param champ_name: champion name
        :param position: selected position (lane)
        :return: formatted key
        """
        return f"{champ_name}^{position}"


app.include_router(router)
