import operator
from copy import deepcopy
from unittest.mock import patch

import pytest
from dict_deep import deep_del, deep_get, deep_set

from config import ItemSets
from models.models import Champion, Item, Items, SkillOrder, SummonerSpell
from opgg_data_helper import OpggDataHelper
from tests.test_data.template_champion_data import BASE_CHAMPION_DATA
from tests.test_data.template_test_data import BASE_MARKUP_WITH_DATA
from tests.test_utils.response_mock import MockResponse


@pytest.fixture
def clean_opgg_helper():
    opgg_helper: OpggDataHelper = OpggDataHelper()
    return opgg_helper


@pytest.fixture
def champion_data_deep_copy():
    return deepcopy(BASE_CHAMPION_DATA)


class TestOpggHelper:

    # test getting available positions
    def test_single_lane_available(self):
        pass

    @patch("aiohttp.ClientSession.request", return_value=MockResponse(BASE_MARKUP_WITH_DATA, 200))
    async def test_multiple_lanes_available(self, clean_opgg_helper):
        available_positions: list[str] = await clean_opgg_helper.get_opgg_available_positions("warwick")
        assert available_positions == ["JUNGLE", "TOP"]

    def test_no_lanes_available(self):
        """Shouldn't happen for real"""
        pass

    # test getting complete data
    @patch("aiohttp.ClientSession.request", return_value=MockResponse(BASE_MARKUP_WITH_DATA, 200))
    async def test_get_champion_help_by_position(self, clean_opgg_helper):
        champion_data: dict = await clean_opgg_helper.get_opgg_champion_help_by_postition("warwick", "jungle")
        assert champion_data == {
            "summoner_spells": [
                SummonerSpell(
                    id=4,
                    key="SummonerFlash",
                    name="Flash",
                    image_url="https://opgg-static.akamaized.net/images/lol/spell/SummonerFlash.png",
                ),
                SummonerSpell(
                    id=11,
                    key="SummonerSmite",
                    name="Smite",
                    image_url="https://opgg-static.akamaized.net/images/lol/spell/SummonerSmite.png",
                ),
            ],
            "skill_order": {
                "priority": ["W", "Q", "E"],
                "order": "Q > W > E > W > W > R > W > Q > W > Q > R > Q > Q > E > E",
            },
            "items": {
                ItemSets.STARTER: [
                    Item(
                        id=1035,
                        name="Emberknife",
                        image_url="https://opgg-static.akamaized.net/images/lol/item/1035.png",
                    ),
                    Item(
                        id=2031,
                        name="Refillable Potion",
                        image_url="https://opgg-static.akamaized.net/images/lol/item/2031.png",
                    ),
                ],
                ItemSets.CORE: [
                    Item(
                        id=3077,
                        name="Tiamat",
                        image_url="https://opgg-static.akamaized.net/images/lol/item/3077.png",
                    ),
                    Item(
                        id=6632,
                        name="Divine Sunderer",
                        image_url="https://opgg-static.akamaized.net/images/lol/item/6632.png",
                    ),
                    Item(
                        id=3748,
                        name="Titanic Hydra",
                        image_url="https://opgg-static.akamaized.net/images/lol/item/3748.png",
                    ),
                    Item(
                        id=3075,
                        name="Thornmail",
                        image_url="https://opgg-static.akamaized.net/images/lol/item/3075.png",
                    ),
                ],
                ItemSets.BOOTS: Item(
                    id=3047,
                    name="Plated Steelcaps",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/3047.png",
                ),
            },
            "runes": {
                "id": 8005,
                "primary_page_id": 8000,
                "secondary_page_id": 8200,
                "play": 438,
                "win": 215,
                "pick_rate": 0.718,
                "builds": [
                    {
                        "id": 8005,
                        "primary_page_id": 8000,
                        "primary_rune_ids": [8005, 9111, 9104, 8299],
                        "secondary_page_id": 8200,
                        "secondary_rune_ids": [8232, 8234],
                        "stat_mod_ids": [5005, 5008, 5002],
                        "play": 344,
                        "win": 165,
                        "pick_rate": 0.7854,
                    },
                    {
                        "id": 8005,
                        "primary_page_id": 8000,
                        "primary_rune_ids": [8005, 9111, 9105, 8299],
                        "secondary_page_id": 8200,
                        "secondary_rune_ids": [8232, 8234],
                        "stat_mod_ids": [5005, 5008, 5002],
                        "play": 75,
                        "win": 39,
                        "pick_rate": 0.1712,
                    },
                    {
                        "id": 8005,
                        "primary_page_id": 8000,
                        "primary_rune_ids": [8005, 9111, 9104, 8299],
                        "secondary_page_id": 8200,
                        "secondary_rune_ids": [8232, 8234],
                        "stat_mod_ids": [5005, 5008, 5003],
                        "play": 9,
                        "win": 6,
                        "pick_rate": 0.0205,
                    },
                    {
                        "id": 8005,
                        "primary_page_id": 8000,
                        "primary_rune_ids": [8005, 9111, 9105, 8014],
                        "secondary_page_id": 8200,
                        "secondary_rune_ids": [8232, 8234],
                        "stat_mod_ids": [5005, 5008, 5003],
                        "play": 4,
                        "win": 2,
                        "pick_rate": 0.0091,
                    },
                    {
                        "id": 8005,
                        "primary_page_id": 8000,
                        "primary_rune_ids": [8005, 9111, 9105, 8299],
                        "secondary_page_id": 8200,
                        "secondary_rune_ids": [8232, 8234],
                        "stat_mod_ids": [5005, 5008, 5003],
                        "play": 4,
                        "win": 2,
                        "pick_rate": 0.0091,
                    },
                ],
            },
            "champion_win_rate": 0.499408,
            "counters": [
                Champion(
                    champion_id=876,
                    play=30,
                    win=18,
                    winrate=0.6,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Lillia.png",
                    key="Lillia",
                    name="Lillia",
                ),
                Champion(
                    champion_id=121,
                    play=44,
                    win=26,
                    winrate=0.59,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Khazix.png",
                    key="Khazix",
                    name="Kha'Zix",
                ),
                Champion(
                    champion_id=203,
                    play=33,
                    win=18,
                    winrate=0.55,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Kindred.png",
                    key="Kindred",
                    name="Kindred",
                ),
                Champion(
                    champion_id=107,
                    play=46,
                    win=25,
                    winrate=0.54,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Rengar.png",
                    key="Rengar",
                    name="Rengar",
                ),
                Champion(
                    champion_id=131,
                    play=113,
                    win=58,
                    winrate=0.51,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Diana.png",
                    key="Diana",
                    name="Diana",
                ),
            ],
            "countered_by": [
                Champion(
                    champion_id=141,
                    play=71,
                    win=34,
                    winrate=0.48,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Kayn.png",
                    key="Kayn",
                    name="Kayn",
                ),
                Champion(
                    champion_id=234,
                    play=64,
                    win=29,
                    winrate=0.45,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Viego.png",
                    key="Viego",
                    name="Viego",
                ),
                Champion(
                    champion_id=154,
                    play=42,
                    win=19,
                    winrate=0.45,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Zac.png",
                    key="Zac",
                    name="Zac",
                ),
                Champion(
                    champion_id=9,
                    play=36,
                    win=16,
                    winrate=0.44,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Fiddlesticks.png",
                    key="Fiddlesticks",
                    name="Fiddlesticks",
                ),
                Champion(
                    champion_id=200,
                    play=47,
                    win=20,
                    winrate=0.43,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Belveth.png",
                    key="Belveth",
                    name="Bel'Veth",
                ),
            ],
        }

    # test get matchups
    def test_matchups_more_than_5_counters_and_countered_by_in_source(self, clean_opgg_helper):
        matchups = clean_opgg_helper._get_matchups_data(BASE_CHAMPION_DATA)
        assert matchups == {
            "counters": [
                Champion(
                    champion_id=876,
                    play=30,
                    win=18,
                    winrate=0.6,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Lillia.png",
                    key="Lillia",
                    name="Lillia",
                ),
                Champion(
                    champion_id=121,
                    play=44,
                    win=26,
                    winrate=0.59,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Khazix.png",
                    key="Khazix",
                    name="Kha'Zix",
                ),
                Champion(
                    champion_id=203,
                    play=33,
                    win=18,
                    winrate=0.55,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Kindred.png",
                    key="Kindred",
                    name="Kindred",
                ),
                Champion(
                    champion_id=107,
                    play=46,
                    win=25,
                    winrate=0.54,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Rengar.png",
                    key="Rengar",
                    name="Rengar",
                ),
                Champion(
                    champion_id=62,
                    play=36,
                    win=19,
                    winrate=0.53,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/MonkeyKing.png",
                    key="MonkeyKing",
                    name="Wukong",
                ),
            ],
            "countered_by": [
                Champion(
                    champion_id=141,
                    play=71,
                    win=34,
                    winrate=0.48,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Kayn.png",
                    key="Kayn",
                    name="Kayn",
                ),
                Champion(
                    champion_id=234,
                    play=64,
                    win=29,
                    winrate=0.45,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Viego.png",
                    key="Viego",
                    name="Viego",
                ),
                Champion(
                    champion_id=154,
                    play=42,
                    win=19,
                    winrate=0.45,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Zac.png",
                    key="Zac",
                    name="Zac",
                ),
                Champion(
                    champion_id=9,
                    play=36,
                    win=16,
                    winrate=0.44,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Fiddlesticks.png",
                    key="Fiddlesticks",
                    name="Fiddlesticks",
                ),
                Champion(
                    champion_id=200,
                    play=47,
                    win=20,
                    winrate=0.43,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Belveth.png",
                    key="Belveth",
                    name="Bel'Veth",
                ),
            ],
        }

    def test_matchups_counters_and_countered_by_single_champion(
        self,
        clean_opgg_helper,
        champion_data_deep_copy,
    ):
        original_matchups: list[dict] = deep_get(champion_data_deep_copy, "props.pageProps.data.summary.counters")
        deep_set(
            champion_data_deep_copy,
            "props.pageProps.data.summary.counters",
            [original_matchups[11], original_matchups[14]],
        )
        matchups = clean_opgg_helper._get_matchups_data(champion_data_deep_copy)
        assert matchups == {
            "counters": [
                Champion(
                    champion_id=876,
                    play=30,
                    win=18,
                    winrate=0.6,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Lillia.png",
                    key="Lillia",
                    name="Lillia",
                ),
            ],
            "countered_by": [
                Champion(
                    champion_id=102,
                    play=34,
                    win=11,
                    winrate=0.32,
                    image_url="https://opgg-static.akamaized.net/images/lol/champion/Shyvana.png",
                    key="Shyvana",
                    name="Shyvana",
                ),
            ],
        }

    def test_matchups_empty_counters_list(self, clean_opgg_helper, champion_data_deep_copy):
        deep_set(champion_data_deep_copy, "props.pageProps.data.summary.counters", [])
        matchups = clean_opgg_helper._get_matchups_data(champion_data_deep_copy)
        assert matchups == {"countered_by": [], "counters": []}

    def test_matchups_counters_field_is_missing(self, clean_opgg_helper, champion_data_deep_copy):
        deep_del(champion_data_deep_copy, "props.pageProps.data.summary.counters")
        matchups = clean_opgg_helper._get_matchups_data(champion_data_deep_copy)
        assert matchups == {"countered_by": [], "counters": []}

    # item sets
    def test_get_complex_item_sets(self, clean_opgg_helper):
        items_sets: Items = clean_opgg_helper._get_items_sets(BASE_CHAMPION_DATA)
        assert items_sets == Items(
            starter_items=[
                Item(
                    id=1035,
                    name="Emberknife",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/1035.png",
                ),
                Item(
                    id=2031,
                    name="Refillable Potion",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/2031.png",
                ),
            ],
            core_items=[
                Item(id=3077, name="Tiamat", image_url="https://opgg-static.akamaized.net/images/lol/item/3077.png"),
                Item(
                    id=6632,
                    name="Divine Sunderer",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/6632.png",
                ),
                Item(
                    id=3748,
                    name="Titanic Hydra",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/3748.png",
                ),
                Item(id=3075, name="Thornmail", image_url="https://opgg-static.akamaized.net/images/lol/item/3075.png"),
            ],
            boots=Item(
                id=3047,
                name="Plated Steelcaps",
                image_url="https://opgg-static.akamaized.net/images/lol/item/3047.png",
            ),
        )

    def test_starter_items_empty(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["starter_items"] = []
        items_sets: Items = clean_opgg_helper._get_items_sets(champion_data_deep_copy)
        assert items_sets == Items(
            starter_items=[],
            core_items=[
                Item(id=3077, name="Tiamat", image_url="https://opgg-static.akamaized.net/images/lol/item/3077.png"),
                Item(
                    id=6632,
                    name="Divine Sunderer",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/6632.png",
                ),
                Item(
                    id=3748,
                    name="Titanic Hydra",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/3748.png",
                ),
                Item(id=3075, name="Thornmail", image_url="https://opgg-static.akamaized.net/images/lol/item/3075.png"),
            ],
            boots=Item(
                id=3047,
                name="Plated Steelcaps",
                image_url="https://opgg-static.akamaized.net/images/lol/item/3047.png",
            ),
        )

    def test_starter_items_missing(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["starter_items"]
        items_sets: Items = clean_opgg_helper._get_items_sets(champion_data_deep_copy)
        assert items_sets == Items(
            starter_items=[],
            core_items=[
                Item(id=3077, name="Tiamat", image_url="https://opgg-static.akamaized.net/images/lol/item/3077.png"),
                Item(
                    id=6632,
                    name="Divine Sunderer",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/6632.png",
                ),
                Item(
                    id=3748,
                    name="Titanic Hydra",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/3748.png",
                ),
                Item(id=3075, name="Thornmail", image_url="https://opgg-static.akamaized.net/images/lol/item/3075.png"),
            ],
            boots=Item(
                id=3047,
                name="Plated Steelcaps",
                image_url="https://opgg-static.akamaized.net/images/lol/item/3047.png",
            ),
        )

    def test_core_items_empty(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["core_items"] = []
        items_sets: Items = clean_opgg_helper._get_items_sets(champion_data_deep_copy)
        assert items_sets == Items(
            starter_items=[
                Item(
                    id=1035,
                    name="Emberknife",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/1035.png",
                ),
                Item(
                    id=2031,
                    name="Refillable Potion",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/2031.png",
                ),
            ],
            core_items=[],
            boots=Item(
                id=3047,
                name="Plated Steelcaps",
                image_url="https://opgg-static.akamaized.net/images/lol/item/3047.png",
            ),
        )

    def test_core_items_missing(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["core_items"]
        items_sets: Items = clean_opgg_helper._get_items_sets(champion_data_deep_copy)
        assert items_sets == Items(
            starter_items=[
                Item(
                    id=1035,
                    name="Emberknife",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/1035.png",
                ),
                Item(
                    id=2031,
                    name="Refillable Potion",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/2031.png",
                ),
            ],
            core_items=[],
            boots=Item(
                id=3047,
                name="Plated Steelcaps",
                image_url="https://opgg-static.akamaized.net/images/lol/item/3047.png",
            ),
        )

    def test_boots_empty(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["boots"] = []
        items_sets: Items = clean_opgg_helper._get_items_sets(champion_data_deep_copy)
        assert items_sets == Items(
            starter_items=[
                Item(
                    id=1035,
                    name="Emberknife",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/1035.png",
                ),
                Item(
                    id=2031,
                    name="Refillable Potion",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/2031.png",
                ),
            ],
            core_items=[
                Item(id=3077, name="Tiamat", image_url="https://opgg-static.akamaized.net/images/lol/item/3077.png"),
                Item(
                    id=6632,
                    name="Divine Sunderer",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/6632.png",
                ),
                Item(
                    id=3748,
                    name="Titanic Hydra",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/3748.png",
                ),
                Item(id=3075, name="Thornmail", image_url="https://opgg-static.akamaized.net/images/lol/item/3075.png"),
            ],
            boots=None,
        )

    def test_boots_missing(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["boots"]
        items_sets: Items = clean_opgg_helper._get_items_sets(champion_data_deep_copy)
        assert items_sets == Items(
            starter_items=[
                Item(
                    id=1035,
                    name="Emberknife",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/1035.png",
                ),
                Item(
                    id=2031,
                    name="Refillable Potion",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/2031.png",
                ),
            ],
            core_items=[
                Item(id=3077, name="Tiamat", image_url="https://opgg-static.akamaized.net/images/lol/item/3077.png"),
                Item(
                    id=6632,
                    name="Divine Sunderer",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/6632.png",
                ),
                Item(
                    id=3748,
                    name="Titanic Hydra",
                    image_url="https://opgg-static.akamaized.net/images/lol/item/3748.png",
                ),
                Item(id=3075, name="Thornmail", image_url="https://opgg-static.akamaized.net/images/lol/item/3075.png"),
            ],
            boots=None,
        )

    # items set
    def test_get_starter_items(self, clean_opgg_helper, champion_data_deep_copy):
        starter_items: list[Item] = clean_opgg_helper._get_champion_itemset(champion_data_deep_copy, ItemSets.STARTER)
        assert starter_items == [
            Item(id=1035, name="Emberknife", image_url="https://opgg-static.akamaized.net/images/lol/item/1035.png"),
            Item(
                id=2031,
                name="Refillable Potion",
                image_url="https://opgg-static.akamaized.net/images/lol/item/2031.png",
            ),
        ]

    def test_get_starter_missing(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["starter_items"]
        starter_items: list[Item] = clean_opgg_helper._get_champion_itemset(champion_data_deep_copy, ItemSets.STARTER)
        assert starter_items == []

    def test_get_starter_empty(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["starter_items"] = []
        starter_items: list[Item] = clean_opgg_helper._get_champion_itemset(champion_data_deep_copy, ItemSets.STARTER)
        assert starter_items == []

    def test_get_core_items(self, clean_opgg_helper, champion_data_deep_copy):
        core_items: list[Item] = clean_opgg_helper._get_champion_itemset(champion_data_deep_copy, ItemSets.CORE)
        assert core_items == [
            Item(id=3077, name="Tiamat", image_url="https://opgg-static.akamaized.net/images/lol/item/3077.png"),
            Item(
                id=6632,
                name="Divine Sunderer",
                image_url="https://opgg-static.akamaized.net/images/lol/item/6632.png",
            ),
            Item(id=3748, name="Titanic Hydra", image_url="https://opgg-static.akamaized.net/images/lol/item/3748.png"),
            Item(id=3075, name="Thornmail", image_url="https://opgg-static.akamaized.net/images/lol/item/3075.png"),
        ]

    def test_get_core_missing(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["core_items"]
        core_items: list[Item] = clean_opgg_helper._get_champion_itemset(champion_data_deep_copy, ItemSets.CORE)
        assert core_items == []

    def test_get_core_empty(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["core_items"] = []
        core_items: list[Item] = clean_opgg_helper._get_champion_itemset(champion_data_deep_copy, ItemSets.CORE)
        assert core_items == []

    def test_get_boots(self, clean_opgg_helper, champion_data_deep_copy):
        boots: list[Item] = clean_opgg_helper._get_champion_itemset(champion_data_deep_copy, ItemSets.BOOTS)
        assert boots == [
            Item(
                id=3047,
                name="Plated Steelcaps",
                image_url="https://opgg-static.akamaized.net/images/lol/item/3047.png",
            ),
        ]

    def test_get_boots_empty(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["boots"] = []
        boots: list[Item] = clean_opgg_helper._get_champion_itemset(champion_data_deep_copy, ItemSets.BOOTS)
        assert boots == []

    def test_get_boots_missing(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["boots"]
        boots: list[Item] = clean_opgg_helper._get_champion_itemset(champion_data_deep_copy, ItemSets.BOOTS)
        assert boots == []

    # filtering matchups
    def test_filter_matchups_gt_above_threshold(self, clean_opgg_helper, champion_data_deep_copy):
        matchups: list[Champion] = [
            Champion(
                champion_id=876,
                play=30,
                win=18,
                winrate=0.6,
                image_url="https://opgg-static.akamaized.net/images/lol/champion/Lillia.png",
                key="Lillia",
                name="Lillia",
            ),
        ]
        counters: list[Champion] = clean_opgg_helper._filter_matchups_by_winrate_threshold(matchups, operator.gt)
        assert counters == matchups

    def test_filter_matchups_gt_below_threshold(self, clean_opgg_helper, champion_data_deep_copy):
        matchups: list[Champion] = [
            Champion(
                champion_id=9,
                play=36,
                win=16,
                winrate=0.44,
                image_url="https://opgg-static.akamaized.net/images/lol/champion/Fiddlesticks.png",
                key="Fiddlesticks",
                name="Fiddlesticks",
            ),
        ]
        counters: list[Champion] = clean_opgg_helper._filter_matchups_by_winrate_threshold(matchups, operator.gt)
        assert counters == []

    def test_filter_matchups_gt_equals_threshold(self, clean_opgg_helper, champion_data_deep_copy):
        matchups: list[Champion] = [
            Champion(
                champion_id=9,
                play=36,
                win=16,
                winrate=0.5,
                image_url="https://opgg-static.akamaized.net/images/lol/champion/Fiddlesticks.png",
                key="Fiddlesticks",
                name="Fiddlesticks",
            ),
        ]
        counters: list[Champion] = clean_opgg_helper._filter_matchups_by_winrate_threshold(matchups, operator.gt)
        assert counters == []

    def test_filter_matchups_lt_below_threshold(self, clean_opgg_helper, champion_data_deep_copy):
        matchups: list[Champion] = [
            Champion(
                champion_id=9,
                play=36,
                win=16,
                winrate=0.45,
                image_url="https://opgg-static.akamaized.net/images/lol/champion/Fiddlesticks.png",
                key="Fiddlesticks",
                name="Fiddlesticks",
            ),
        ]
        counters: list[Champion] = clean_opgg_helper._filter_matchups_by_winrate_threshold(matchups, operator.lt)
        assert counters == [
            Champion(
                champion_id=9,
                play=36,
                win=16,
                winrate=0.45,
                image_url="https://opgg-static.akamaized.net/images/lol/champion/Fiddlesticks.png",
                key="Fiddlesticks",
                name="Fiddlesticks",
            ),
        ]

    def test_filter_matchups_lt_equals_threshold(self, clean_opgg_helper, champion_data_deep_copy):
        matchups: list[Champion] = [
            Champion(
                champion_id=9,
                play=36,
                win=16,
                winrate=0.5,
                image_url="https://opgg-static.akamaized.net/images/lol/champion/Fiddlesticks.png",
                key="Fiddlesticks",
                name="Fiddlesticks",
            ),
        ]
        counters: list[Champion] = clean_opgg_helper._filter_matchups_by_winrate_threshold(matchups, operator.lt)
        assert counters == []

    def test_filter_matchups_lt_above_threshold(self, clean_opgg_helper, champion_data_deep_copy):
        matchups: list[Champion] = [
            Champion(
                champion_id=9,
                play=36,
                win=16,
                winrate=0.51,
                image_url="https://opgg-static.akamaized.net/images/lol/champion/Fiddlesticks.png",
                key="Fiddlesticks",
                name="Fiddlesticks",
            ),
        ]
        counters: list[Champion] = clean_opgg_helper._filter_matchups_by_winrate_threshold(matchups, operator.lt)
        assert counters == []

    # summoner spells
    def test_get_summoner_spells(self, clean_opgg_helper, champion_data_deep_copy):
        summoner_spells: list[SummonerSpell] = clean_opgg_helper._get_summoner_spells_data(champion_data_deep_copy)
        assert summoner_spells == [
            SummonerSpell(
                id=4,
                key="SummonerFlash",
                name="Flash",
                image_url="https://opgg-static.akamaized.net/images/lol/spell/SummonerFlash.png",
            ),
            SummonerSpell(
                id=11,
                key="SummonerSmite",
                name="Smite",
                image_url="https://opgg-static.akamaized.net/images/lol/spell/SummonerSmite.png",
            ),
        ]

    def test_get_summoner_spells_missing(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["summoner_spells"] = []
        summoner_spells: list[SummonerSpell] = clean_opgg_helper._get_summoner_spells_data(champion_data_deep_copy)
        assert summoner_spells == []

    def test_get_summoner_spells_empty(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["summoner_spells"]
        summoner_spells: list[SummonerSpell] = clean_opgg_helper._get_summoner_spells_data(champion_data_deep_copy)
        assert summoner_spells == []

    # skill order
    def test_get_skill_order(self, clean_opgg_helper, champion_data_deep_copy):
        skill_order: SkillOrder = clean_opgg_helper._get_skill_order(champion_data_deep_copy)
        assert skill_order == SkillOrder(
            priority=["W", "Q", "E"],
            order="Q > W > E > W > W > R > W > Q > W > Q > R > Q > Q > E > E",
        )

    def test_get_skill_order_ids_missing(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["skill_masteries"][0]["ids"]
        skill_order: SkillOrder = clean_opgg_helper._get_skill_order(champion_data_deep_copy)
        assert skill_order == SkillOrder(
            priority=[],
            order="Q > W > E > W > W > R > W > Q > W > Q > R > Q > Q > E > E",
        )

    def test_get_skill_order_priority_missing(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["skill_masteries"][0]["builds"][0]["order"]
        skill_order: SkillOrder = clean_opgg_helper._get_skill_order(champion_data_deep_copy)
        assert skill_order == SkillOrder(
            priority=["W", "Q", "E"],
            order="",
        )

    def test_get_skill_order_ids_empty(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["skill_masteries"][0]["ids"] = []
        skill_order: SkillOrder = clean_opgg_helper._get_skill_order(champion_data_deep_copy)
        assert skill_order == SkillOrder(
            priority=[],
            order="Q > W > E > W > W > R > W > Q > W > Q > R > Q > Q > E > E",
        )

    def test_get_skill_order_priority_empty(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["skill_masteries"][0]["builds"][0]["order"] = []
        skill_order: SkillOrder = clean_opgg_helper._get_skill_order(champion_data_deep_copy)
        assert skill_order == SkillOrder(
            priority=["W", "Q", "E"],
            order="",
        )

    def test_get_skill_order_skill_masteries_empty(self, clean_opgg_helper, champion_data_deep_copy):
        champion_data_deep_copy["props"]["pageProps"]["data"]["skill_masteries"] = []
        skill_order: SkillOrder = clean_opgg_helper._get_skill_order(champion_data_deep_copy)
        assert skill_order == SkillOrder(
            priority=[],
            order="",
        )

    def test_get_skill_order_skill_masteries_missing(self, clean_opgg_helper, champion_data_deep_copy):
        del champion_data_deep_copy["props"]["pageProps"]["data"]["skill_masteries"]
        skill_order: SkillOrder = clean_opgg_helper._get_skill_order(champion_data_deep_copy)
        assert skill_order == SkillOrder(
            priority=[],
            order="",
        )
