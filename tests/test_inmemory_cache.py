from time import sleep
from unittest.mock import patch

import pytest

from cache.inmemory_cache import InMemoryCache
from models.models import Item


@pytest.fixture
def clean_cache():
    cache: InMemoryCache = InMemoryCache()
    cache.reset()
    return cache


class TestInMemoryCache:

    def test_get_key_exists(self, clean_cache):
        test_key, test_value = "test", {"1": 2}
        clean_cache.put(test_key, test_value)
        assert clean_cache.get(test_key) == test_value

    def test_get_key_not_exists(self, clean_cache):
        assert clean_cache.get("test2") is None

    def test_get_key_as_result_model(self, clean_cache):
        test_key: str = "test3"
        raw_test_value: dict = {"id": 123, "name": "name", "image_url": "img_url"}
        clean_cache.put(test_key, raw_test_value)
        assert clean_cache.get(test_key, Item) == Item(**raw_test_value)

    def test_get_key_as_dict(self, clean_cache):
        test_key: str = "test4"
        raw_test_value: dict = {"id": 123, "name": "name", "image_url": "img_url"}
        clean_cache.put(test_key, raw_test_value)
        assert clean_cache.get(test_key) == raw_test_value

    def test_put_dict(self, clean_cache):
        test_key: str = "test5"
        raw_test_value: dict = {"id": 123, "name": "name", "image_url": "img_url"}
        clean_cache.put(test_key, raw_test_value)
        assert clean_cache.get(test_key) == raw_test_value

    def test_put_model(self, clean_cache):
        test_key: str = "test6"
        raw_test_value: dict = {"id": 123, "name": "name", "image_url": "img_url"}
        clean_cache.put(test_key, Item(**raw_test_value))
        assert clean_cache.get(test_key) == raw_test_value

    @patch("cache.inmemory_cache.CACHE_EXPIRATION_SECONDS", 3)
    def test_expired_record_not_returned(self, clean_cache):
        test_key: str = "test8"
        raw_test_value: dict = {"id": 123, "name": "name", "image_url": "img_url"}
        clean_cache.put(test_key, raw_test_value)
        assert clean_cache.get(test_key) == raw_test_value
        sleep(4)
        assert clean_cache.get(test_key) is None

    def test_reset(self, clean_cache):
        test_key: str = "test9"
        raw_test_value: dict = {"id": 123, "name": "name", "image_url": "img_url"}
        clean_cache.put(test_key, raw_test_value)
        clean_cache.reset()
        assert clean_cache._cache == {}

    def test_cache_is_singleton(self, clean_cache):
        cache1: InMemoryCache = InMemoryCache()
        cache2: InMemoryCache = InMemoryCache()
        assert cache1 is cache2
